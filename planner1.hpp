#ifndef PLANNER1_HPP
#define PLANNER1_HPP

#include "world.hpp"
#include "priority_queue.hpp"
#include <vector>
#include <unordered_map>
#include <queue>
#include <iomanip>
#include <fstream>


class Planner1
{
public:
	Planner1(const World& world);
	~Planner1();

	void GenerateHeuristics();

	void Plan();

	void SetWeight(const float& w) { weight_ = w; };
	
private:

	World world_;

	int target_count_;

	float weight_;

	// 				ID, cost
	std::unordered_map<int, float> map_Heuristics_;

	// 				ID in 3d, Point
	std::unordered_map<double, Point> map_id_point_;

	void ReconstructPath(std::unordered_map<double,double>& cameFrom);

	int AchievableGoalIdx();

	void InsertPossibleGoals(PQ_pt& container, const int& idx);


};


#endif