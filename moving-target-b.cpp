#include <iostream>
#include <ctime>
#include "world.hpp"
#include "planner1.hpp"

// #include <opencv2/core/mat.hpp>


int main(int argc, char const *argv[])
{
	/* parsing input txt file */
	std::string file_name;

	if (argc > 1)
		file_name = argv[1];

	std::ifstream file(file_name.c_str());

	std::string input ((std::istreambuf_iterator<char>(file)), (std::istreambuf_iterator<char>()));

	/**********************/
	/*      Parsing       */
	/**********************/
	World world;

	world.ParsingText(input);
	

	/**********************/
	/*      Planning      */
	/**********************/
	Planner1 planner(world);

	planner.SetWeight(10);

	
	/*************************/
	/*  Generating heuristic */
	/*************************/
	std::clock_t heuristics_start;

	planner.GenerateHeuristics();
	
	std::cout << (std::clock() - heuristics_start) / static_cast<double>(CLOCKS_PER_SEC) << " secs" << std::endl << std::endl;


	/*************************/
	/*  Backward A* search   */
	/*************************/
	std::clock_t planner_start;

	planner.Plan();

	std::cout << (std::clock() - planner_start) / static_cast<double>(CLOCKS_PER_SEC) << " secs" << std::endl << std::endl;


	return 0;
}



