#include "world.hpp"


World::World():
	map_built_(false)
{

}


World::~World()
{
	
}


void World::ParsingWords(const std::vector<std::string>& vec_lines)
{
	// parsing the grid size
	int grid_size = 0;
	auto grid_size_search_it = std::find(vec_lines.begin(), vec_lines.end(), "N");
	if ( grid_size_search_it != vec_lines.end()){

	  	grid_size = stoi(*(grid_size_search_it+1));
	  	// std::cout << "grid_size: " << grid_size << std::endl;
	}

	// parsing the robot init pos
	
	auto robot_init_search_it = std::find(vec_lines.begin(), vec_lines.end(), "R");
	if ( robot_init_search_it != vec_lines.end()){
	  	
		robot_trajectory.push_back(XYString2Point(*(robot_init_search_it+1), 0));

	  	// PrintPoint(robot_trajectory[0]);
	}

	auto target_traj_search_it = std::find(vec_lines.begin(), vec_lines.end(), "T");
	auto cost_traj_search_it = std::find(vec_lines.begin(), vec_lines.end(), "B");
	
	int time_count = 0;
	for (auto it = target_traj_search_it+1; it != cost_traj_search_it; ++it){
		
		target_trajectory.push_back(XYString2Point(*it, time_count++));

		// PrintPoint(target_trajectory[time_count-1]);
	}

	for (int i = std::distance(vec_lines.begin(),cost_traj_search_it+1); i < vec_lines.size(); ++i){
		
		std::vector<std::string> row;
		boost::split(row, vec_lines[i], boost::is_any_of(","));

		int row_count = 0;

		std::vector<int> row_cost;

		for (int j = 0; j < row.size(); ++j){

			row_cost.push_back(stoi(row[j]));
			
		}

		cost_map.push_back(row_cost);

	}

	// PrintCostMap(cost_map);

}


void World::ParsingLines(std::vector<std::string>& vec_lines, const std::string& text_input)
{
	// idx for line searching
	int line_idx = -1;
	// count how many variables, this value will be the index
	int counter = 0;

	do{
		int starting_pos = ++line_idx;
		// searching for lines
		line_idx = text_input.find("\n",starting_pos);

		if (line_idx != -1){

			// the whole line of text
			auto text = text_input.substr(starting_pos,line_idx-starting_pos);
			text.erase(std::remove(text.begin(), text.end(), ' '), text.end());

			vec_lines.push_back(text);
			// std::cout << text << std::endl;
			
		}
	} while (line_idx != -1);
}


void World::ParsingText(const std::string& text_input)
{

	std::vector<std::string> vec_lines;
	
	ParsingLines(vec_lines, text_input);

	ParsingWords(vec_lines);

	map_built_ = true;
	
}


int World::GridCoordToNodeId(const Point& pt)
{
	return pt.x*cost_map[0].size() + pt.y;
}


Point World::NodeIdToGridCoord(const int& id, const int& time = 0)
{
	Point pt;
	pt.x = id / static_cast<int>(cost_map[0].size());
	pt.y = id - cost_map[0].size()*pt.x;
	pt.t = time;
	return pt;
}


std::vector<int> World::GetNeighborID(const int& id, const bool stay = false)
{
	Point pt = this->NodeIdToGridCoord(id);

	// std::vector<std::vector<int>> moves{{0,0}, {1,0}, {0,1}, {0,-1}, {-1,0}};
	std::vector<std::vector<int>> moves{{1,0}, {0,1}, {0,-1}, {-1,0}};

	if(stay)
		moves.push_back({0,0});

	std::vector<int> output;

	for(auto action : moves){
		if(pt.x + action[0] < cost_map.size() && pt.y - action[1] < cost_map[0].size()){
			Point neighbor(pt.x + action[0], pt.y - action[1]);
			output.push_back(this->GridCoordToNodeId(neighbor));
		}
	}
	return output;
}


double World::GridCoordToNodeId_3D(const Point& pt)
{
	return pt.x*cost_map[0].size() + pt.y + float(pt.t)/10000.0;
}


std::vector<Point> World::Get3DNeighborID(const Point& pt, bool stay)
{
	Point pt_goal(0,0,0);

	std::vector<std::vector<int>> moves{{1,0,-1}, {0,1,-1}, {0,-1,-1}, {-1,0,-1}};

	if(stay)
		moves.push_back({0,0,-1});

	std::vector<Point> output;

	for(auto action : moves){
		if(pt.x + action[0] < cost_map.size() && pt.y + action[1] < cost_map[0].size()){

			Point pt_neighbor(pt.x + action[0], pt.y + action[1], pt.t + action[2]);

			// *** optimization trick ***
			// only exapnd if it's reachable in time
			if(pt_neighbor.t >= this->ManhattanDistance(pt_goal,pt_neighbor)){
				output.push_back(pt_neighbor);
			}
		}
	}
	return output;
}
