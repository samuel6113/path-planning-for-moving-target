#ifndef PRIORITY_QUEUE_HPP
#define PRIORITY_QUEUE_HPP

#include <vector>
#include <unordered_map>
#include <queue>

typedef std::pair<float, int> pCostNode;
typedef std::pair<float, Point> pCostPoint;

struct PQ
{
	std::priority_queue<pCostNode, std::vector<pCostNode>, std::greater<pCostNode>> element;

	inline bool empty() { return element.empty(); };

	inline void put(const float& cost, const int& node_id){
		element.push(pCostNode(cost,node_id));
	}

	inline int get(){
		int output = element.top().second;
		element.pop();
		return output;
	}

	inline int getTopID(){
		return element.top().second;
	}
};


class PtCompare
{
public:
    bool operator() (const pCostPoint& lhs, const pCostPoint& rhs)
    {
        return lhs.first > rhs.first;
    }
};
// http://stackoverflow.com/questions/5807735/c-priority-queue-with-lambda-comparator-error
// First define the lambda object, 
// then pass it to the template's type using decltype 
// and also pass it directly to the constructor.

// auto PtCompare = [](const pCostPoint& lhs, const pCostPoint& rhs) { return lhs.first > rhs.first; };

struct PQ_pt
{
	std::priority_queue<pCostPoint, std::vector<pCostPoint>, PtCompare> element;

	inline bool empty() { return element.empty(); };

	inline void put(const float& cost, const Point& pt){
		element.push(pCostPoint(cost,pt));
	}

	inline Point get(){
		Point output = element.top().second;
		element.pop();
		return output;
	}

	inline Point topPt(){
		return element.top().second;
	}

	inline float topCost(){
		return element.top().first;
	}
};

#endif