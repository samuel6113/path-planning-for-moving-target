CXX=g++
CFLAGS=-std=c++11
INCLUDE=-I/opt/local/var/macports/software/boost/opt/local/include\
		-I/usr/local/opt/opencv3/include

LDFLAGS=-L/usr/local/opt/opencv3/lib

DEPEND=world.cpp\
		planner1.cpp

EXECUTABLE=moving-target-a.cpp\
			moving-target-b.cpp

EXECUTABLES:=$(basename $(EXECUTABLE))

all: ${EXECUTABLES}

$(EXECUTABLES): % : %.cpp
	$(CXX) $(CFLAGS) $(LDFLAGS) $^ $(INCLUDE) $(DEPEND) -o $@

clean:
	rm ${EXECUTABLES}
