#ifndef WORLD_HPP
#define WORLD_HPP

#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <boost/algorithm/string.hpp>

struct Point
{
	int x,y,t;
	Point(int a, int b, int c = 0){
		x = a;
		y = b;
		t = c;
	};

	Point() {};
};


class World
{
public:
	World();
	~World();

	std::vector<Point> robot_trajectory;

	std::vector<Point> target_trajectory;

	std::vector<std::vector<int>> cost_map;

	void ParsingText(const std::string& text_input);

	int GridCoordToNodeId(const Point& pt);

	Point NodeIdToGridCoord(const int& id, const int& time);

	std::vector<int> GetNeighborID(const int& id, bool stay);

	double GridCoordToNodeId_3D(const Point& pt);

	std::vector<Point> Get3DNeighborID(const Point& pt, bool stay);

	inline int GetCost(const int& id) { 
		auto pt = NodeIdToGridCoord(id,0);
		return cost_map[pt.x][pt.y];
	};

	inline int ManhattanDistance(const Point& pt1, const Point& pt2){
		return std::abs(pt2.y-pt1.y) + std::abs(pt2.x-pt1.x);
	};

private:

	bool map_built_;

	void ParsingLines(std::vector<std::string>& vec_lines, const std::string& text_input);

	void ParsingWords(const std::vector<std::string>& vec_lines);

};

inline Point XYString2Point(const std::string& str, const int& time)
{
	Point point;

	std::vector<std::string> vec_string;
	boost::split(vec_string, str, boost::is_any_of(","));

	point.x = stoi(vec_string[0]);
	point.y = stoi(vec_string[1]);
	point.t = time;

	return point;
}


static inline void PrintPoint(const Point& pt)
{
	std::cout << pt.x << "," << pt.y << " @ t = " << pt.t << std::endl;
}


static inline void PrintCostMap(const std::vector<std::vector<int>>& cost_map)
{
	for (auto row : cost_map)
	{
		for(auto elem : row){
			std::cout << elem << ",";
		}

		std::cout << std::endl;
	}
}

static inline bool operator== (const Point& pt1, const Point& pt2)
{
	if (pt1.x == pt2.x && pt1.y == pt2.y && pt1.t == pt2.t){
		return true;
	}
	return false;
}

#endif