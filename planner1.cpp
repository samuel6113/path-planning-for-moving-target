#include "planner1.hpp"


Planner1::Planner1(const World& world) : 
	world_(world),
	weight_(1.0)
{

}


Planner1::~Planner1()
{

}

void Planner1::GenerateHeuristics()
{

	auto start_id = world_.GridCoordToNodeId(world_.robot_trajectory[0]);

	PQ open;

	open.put(0.0, start_id);

	//(ID, cost)
	map_Heuristics_[start_id] = 0.0;


	while(!open.empty()){

		auto current_id = open.get();	

		for (auto next : world_.GetNeighborID(current_id,false)){
 
			float new_cost = map_Heuristics_[current_id] + world_.GetCost(next);

			if ( (!map_Heuristics_.count(next)) || (new_cost < map_Heuristics_[next]) )
			{
				map_Heuristics_[next] = new_cost;

				open.put(new_cost,next);
			}
		}
	}

	std::cout << "*** Heurisitics Generated ***" << std::endl;
}


void Planner1::Plan()
{
	int idx = this->AchievableGoalIdx();

	PQ_pt open;

	this->InsertPossibleGoals(open, idx);

	Point first_top = open.topPt(); 
	int first_top_id = world_.GridCoordToNodeId_3D(first_top);

	map_id_point_[first_top_id] = first_top;
	
	//    		(child, parent)
	std::unordered_map<double,double> map_CameFrom;
	map_CameFrom[first_top_id] = first_top_id;

	// 					(id, cost)
	std::unordered_map<double, float> map_cost_to_come; 
	map_cost_to_come[first_top_id] = 0; 

	// *** optimization trick ***
	// only choose to stay in the same cell 
	// if the cost of this cell is by far the smallest
	// otherwise the optimal path would choose to stay at the previous minimum
	// set another boolean to indicate to expand the same grid or not
	// bool stay_or_not = true;
	int by_far_min = std::numeric_limits<int>::max();

	int count = 0;

	while(!open.empty()){

		float topCost = open.topCost();
		Point current_pt = open.get();
		double current_id = world_.GridCoordToNodeId_3D(current_pt);


		if (current_pt == world_.robot_trajectory[0]){
			std::cout << "*** Found goal ***" << std::endl;
			std::cout << "Total Cost: " << map_cost_to_come[world_.GridCoordToNodeId_3D(current_pt)]+world_.GetCost(first_top_id) << std::endl;
			std::cout << "Node Expansion: " << count << std::endl;
			ReconstructPath(map_CameFrom);
			break;
		}

		for (auto next_pt : world_.Get3DNeighborID(current_pt,true)){

			double next_ptID = world_.GridCoordToNodeId_3D(next_pt);

			// if the neighbor is to stay, check if it's the min or not
			// if not, no need to stay, thus no need to expand
			if(static_cast<int>(next_ptID) == static_cast<int>(current_id)){
				if(world_.GetCost(next_ptID) > by_far_min){
					continue;
				}else{
					by_far_min = world_.GetCost(next_ptID);
				}
			}
				

			map_id_point_[next_ptID] = next_pt;

			float g_cost = world_.GetCost(next_ptID) + map_cost_to_come[current_id];

			if ( !map_cost_to_come.count(next_ptID) || g_cost < map_cost_to_come[next_ptID]) {

				map_cost_to_come[next_ptID] = g_cost;

				float f_cost = g_cost + weight_*map_Heuristics_[static_cast<int>(next_ptID)] - weight_*world_.GetCost(next_ptID);

				open.put(f_cost,next_pt);

				map_CameFrom[next_ptID] = current_id;
			}
		}
		++count;
	}

	std::cout << "Arrive at: " << std::endl;
	PrintPoint(world_.target_trajectory[target_count_]);
	std::cout << std::endl;
	std::cout << "*** Search Done ***" << std::endl ;


}

void Planner1::InsertPossibleGoals(PQ_pt& container, const int& idx)
{
	for (int i = idx; i < world_.target_trajectory.size(); ++i){
		auto id = world_.GridCoordToNodeId(world_.target_trajectory[i]);
		float cost = weight_*map_Heuristics_[id];
		container.put(cost, world_.target_trajectory[i]);
	}
}


int Planner1::AchievableGoalIdx()
{
	// TODO can use binary search to improve
	for (int i = 0; i < world_.target_trajectory.size(); ++i){
		if(i >= world_.ManhattanDistance(world_.robot_trajectory[0], world_.target_trajectory[i]))
			return i;
	}

	return -1;
}


void Planner1::ReconstructPath(std::unordered_map<double,double>& cameFrom)
{
	double current = world_.GridCoordToNodeId_3D(world_.robot_trajectory[0]);

	std::ofstream output_file;

	output_file.open ("result_path.txt");

	while (cameFrom.count(current)){

		output_file << map_id_point_[current].x << "," << map_id_point_[current].y << " @ t = " << map_id_point_[current].t << std::endl;
		current = cameFrom[current];
		++target_count_;
	}

	output_file << world_.target_trajectory[target_count_].x << "," << world_.target_trajectory[target_count_].y << " @ t = " << world_.target_trajectory[target_count_].t << std::endl;

	output_file.close();

}


