# Path Planning for Moving Target #
![Screen Shot 2016-10-22 at 2.12.15 PM.png](https://bitbucket.org/repo/XqER7j/images/3722666282-Screen%20Shot%202016-10-22%20at%202.12.15%20PM.png)

* This repo is about how to plan a path for a robot to catch a moving target (think like a Pac Man game)
* The robot and the target live in a 4-connected grid world and the robot can choose to stay in the same grid
* The target trajectory is given (more than 2000 steps)
* The map size is 1000x1000 and has non-uniform cost (see below for visualization)


# Approach #

* In order to catch the target in time, this problem becomes a 3D planning problem. However, searching in 3 dimension is very expensive and not practical. Instead, we can choose to solve a lower dimensional problem first and use this result to guide the high dimensional search.
### **Planner A**: Optimal cost ###
* 2D Dykstra is used first to generate heuristic for the whole map. A backward 3D A* search with several optimization tricks is performed.
* With optimization, the number of node expanded decrease from **2,353,147** to **636,045**. 
* Optimization trick 1: Only expand the target trajectory that is achievable by the robot. If the manhattan distance between two points are larger than the time step, they won't be able to meet in time.
* Optimization trick 2: To plan a path with optimal cost, the robot should only stay in the same grid if that grid has by far the lowest cost.
### **Planner B**: Faster search ###
* At the expense of optimality, heuristic is weighted to speed up the search.
* With weight of 10, the planner only expands **1974** nodes and the cost increase 14.58%.

# Usage #

* Planner A: **./moving-target-a problem_1.txt**
* Planner B: **./moving-target-b problem_1.txt**
* Makefile is also provided.


# Environment #

* C++
* Boost

![Selection_116.png](https://bitbucket.org/repo/XqER7j/images/1366066417-Selection_116.png)